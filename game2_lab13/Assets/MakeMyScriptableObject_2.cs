﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class MakeMyScriptableObject_2 : MonoBehaviour
{
    [MenuItem("Assets/Create/My Scriptable Object2")]
    // Start is called before the first frame update
  public static void CreateMyAsset()
 {
 MyScriptableObject_2 asset = ScriptableObject.CreateInstance <MyScriptableObject_2 >();

 AssetDatabase.CreateAsset(asset, "Assets/NewScripableObject_2.asset");
 AssetDatabase.SaveAssets();

 EditorUtility.FocusProjectWindow();

 Selection.activeObject = asset;
}
}