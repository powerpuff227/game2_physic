﻿using UnityEngine;
public class MyScriptableObject : ScriptableObject
{
    public string _objectName = "my object";
    public int Rotate = 0;
    //public float _fireRate = 1.0f; //The rate at which the gun is fired.
}