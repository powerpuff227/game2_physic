﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMyScriptableObject_2 : MonoBehaviour
{
    public int rotate = 0;
    
    [SerializeField]
     MyScriptableObject_2 _myScriptableObject_2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,rotate,0);
    }

     
}

