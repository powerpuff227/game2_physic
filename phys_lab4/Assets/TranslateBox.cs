﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateBox : MonoBehaviour
{
   [SerializeField]
    private Vector3 _force;
    public Vector3 Force
    {
        get
        {
            return _force;
        }
        set
        {
            _force = value;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       

    }

     private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Player")
            ApplyForce();

    }

    private void ApplyForce()
    {
        Rigidbody rb = this.GetComponent<Rigidbody>();
        rb.AddForce(_force);
    }
}