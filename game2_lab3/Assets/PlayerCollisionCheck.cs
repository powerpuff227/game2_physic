﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerCollisionCheck : MonoBehaviour {
    // Start is called before the first frame update
    public TextMeshPro _textMeshPro = null;
    /*TextMeshPro _textMeshPro = null;*/
    
    void Start () {
          /*   _textMeshPro = this.GetComponentInChildren<_textMeshPro>*/
    }

    // Update is called once per frame
    void Update () {

    }

    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "Item") 
        {
            Destroy (collision.gameObject);
        } else if (collision.gameObject.tag == "Obstacle") {
            Destroy (collision.gameObject, 0.5f);
        }

       /*  TextMesh TextMesh = this.gameObject.GetComponentInChildren<TextMesh>();
        this.transform.chi*/
        
        ItemTypeComponent itc = collision.gameObject.GetComponent<ItemTypeComponent>();
        if(itc != null){
            switch(itc.Type){
                case ItemType.BOX_ITEM:
                 _textMeshPro.text = "Box Item";                
                break;

                case ItemType.SPHERE_OBSTACLE:
                _textMeshPro.text = "Sphere Item";
                break;

                case ItemType.Cylinder_Obstacle:
                _textMeshPro.text = "Cylinder Item";
                break;

                 case ItemType.Capsule_Obstacle:
                _textMeshPro.text = "Capsule Item";
                break;


                
                
             
            }
        }
    }
}