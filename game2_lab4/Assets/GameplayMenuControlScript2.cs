﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayMenuControlScript2 : MonoBehaviour {
   // [SerializeField] Button _backButton;
   [SerializeField] Button _backButton2;

    // Start is called before the first frame update
    void Start () {
     //   _backButton.onClick.AddListener (delegate { BackToMainMenuButtonClick (_backButton); });
        _backButton2.onClick.AddListener (delegate { BackToMainMenuButtonClick2 (_backButton2); });
    }
    

    // Update is called once per frame
    void Update () {

    }



    public void BackToMainMenuButtonClick2 (Button button2) {
      

         
        SceneManager.UnloadSceneAsync ("Game");
        SceneManager.LoadScene ("SceneMainMenu");
    }
}